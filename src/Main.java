public class Main extends Thread {

    public void run() {
        System.out.println("Hello from a thread!" + getName());
    }

    public static void main(String args[]) {
        Thread thread = Thread.currentThread();
        for (int i = 0; i < 10; i++) {
            (new Main()).start();
        }
        System.out.println("Hello from a main thread! " + thread.getName());
    }
}
